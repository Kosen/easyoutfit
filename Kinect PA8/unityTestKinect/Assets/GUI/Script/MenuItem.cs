﻿using UnityEngine;
using System.Collections;

public class MenuItem : MonoBehaviour {
	
	public float timeToComplete = 2;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnSelected(bool onselected) 
	{
		if(onselected)
		{
			//iTween.MoveTo(gameObject,Vector3(0,0,-1),timeToComplete);
			iTween.MoveTo(gameObject,new Vector3(0,0,-1),timeToComplete);
		}
		else
		{
			iTween.MoveTo(gameObject,new Vector3(0,0,0),timeToComplete);
		}
	}
}
