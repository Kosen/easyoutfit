﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class menuScript : MonoBehaviour {
	
	public MenuItem[] listeVetement;
	private int currentMenuIndex = 0;
	private string hilightedClothName = "SelectedCloth";
	
	// Use this for initialization
	void Start () {
		
		/*listElem = listMenuElement.GetComponentsInChildren<MenuItem>;
		foreach(MenuItem child in listElem)
		{
				listeElement.Add(child);
		}*/
		Debug.Log("(1) currentMenuIndex :"+currentMenuIndex);
		Debug.Log("(1) taille liste :"+listeVetement.Length);
		listeVetement[currentMenuIndex].OnSelected(true);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetAxisRaw("Horizontal") > 0.9)
		{
			currentMenuIndex--;
			if (currentMenuIndex <0)
			{
				currentMenuIndex = listeVetement.Length -1;
			}
		}
		else if(Input.GetAxisRaw("Horizontal") < -0.9)
		{
			currentMenuIndex ++;
			if (currentMenuIndex > listeVetement.Length -1)
			{
				currentMenuIndex = 0;
			}
		}
		Debug.Log("(2) currentMenuIndex :"+currentMenuIndex);
		listeVetement[currentMenuIndex].OnSelected(true);
		transform.FindChild(hilightedClothName).renderer.material.mainTexture = listeVetement[currentMenuIndex].renderer.material.mainTexture;
	}
}
