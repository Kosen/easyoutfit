﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class followPath : MonoBehaviour {
	
	public GameObject ReferenceListeVetement;
	public string pathName = "PathCloth";
	public float timeToComplete = 5;
	
	private List<Transform> listeHabit = new List<Transform>();
	private float pas;
	
	// Use this for initialization
	void Start () 
	{
		
		foreach(Transform child in ReferenceListeVetement.transform)
		{
				listeHabit.Add(child);
		}
		
		Debug.Log("Taille de la liste :" + listeHabit.Count);
		
		pas = 1f/ listeHabit.Count;
		
		Debug.Log("le pas est de :" + pas);
		
		int  i = 0;
		foreach(Transform elem in listeHabit)
		{
			iTween.PutOnPath(elem,iTweenPath.GetPath(pathName),i*pas);
			i++;
		}
		
		
	}
	
	// Update is called once per frame
	void Update () {
	}
}
