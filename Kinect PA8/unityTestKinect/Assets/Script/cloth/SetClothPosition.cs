﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class SetClothPosition : MonoBehaviour {
	public SkeletonWrapper sw;
	
	public GameObject Shoulder_Right;
	public GameObject Shoulder_Left;
	public GameObject Spine;
	
	public Cloth InteractiveCloth;
	public GameObject ReferencelistCollider;
	private GameObject current; //need initialisation
	
	private List<Transform> listCollider = new List<Transform>();
	private int tailleList;
	private bool skeletonTracked;
	private bool visible;
	
	//Parametre iamge
	private double LargeurInitImg;
	private double LongueurInitImg;
	private double ProfondeurInitImg;
	
	private double RatioLargeur;
	private double RatioLongeur;
	private double RatioProfondeur;
	
	// Use this for initialization
	void Start () {
		current= gameObject;
		Debug.Log ("le nom de mon obj est : " +current.name);
			
		//Creation liste collider
		foreach(Transform child in ReferencelistCollider.transform)
		{
				listCollider.Add(child);
		}
		tailleList = listCollider.Count;
		Debug.Log("Taille de la liste :" + tailleList);
		
		//renseignement des variables d'image
		LargeurInitImg = InteractiveCloth.transform.localScale.x;
		LongueurInitImg = InteractiveCloth.transform.localScale.y;
		ProfondeurInitImg = InteractiveCloth.transform.localScale.z;
		
		RatioLargeur = LargeurInitImg / Math.Abs(listCollider[1].transform.position.x - listCollider[0].transform.position.x);
		RatioLongeur = LongueurInitImg / Math.Abs(listCollider[1].transform.position.y - listCollider[3].transform.position.y);
		RatioProfondeur = ProfondeurInitImg /  Math.Abs(listCollider[2].transform.position.z - listCollider[3].transform.position.z);
		
		//Masquer le prefab
		visible = false;
		setVisible (visible);
	}
	
	// Update is called once per frame
	void Update () {
		
		skeletonTracked = false;
		
		foreach (int i in sw.trackedPlayers)
		{
			//Attention kinect peux traker totalement 2 personne simultanement
			if (i >= 0){ skeletonTracked = true;}
		}
		
		if (skeletonTracked)
		{
			Debug.Log ("Detection squelette : ");
			if (visible == false)
			{
				visible = true;
				setVisible (visible);
			}
			moveCollider();
		}
		else
		{
			Debug.LogError ("Echec de la Detection");
			if (visible == true)
			{
				visible = false;
				setVisible (visible);
			}
		}
	}
	
	void setVisible (bool visibilityState)
		{
			foreach(Transform coll in listCollider)
			{
					coll.renderer.enabled  = visibilityState;
			}
			InteractiveCloth.renderer.enabled  = visibilityState;
		}
	void moveCollider()
		{
			listCollider[0].transform.localScale = Shoulder_Right.transform.localScale;
			listCollider[1].transform.localScale  = Shoulder_Left.transform.localScale;
			listCollider[2].transform.localScale = Spine.transform.localScale;
			
			listCollider[0].transform.position = Shoulder_Right.transform.position;
			listCollider[1].transform.position  = Shoulder_Left.transform.position;
			listCollider[2].transform.position = Spine.transform.position;
			
			float dx = listCollider[1].transform.position.x - Shoulder_Right.transform.position.x;
			float dy = listCollider[1].transform.position.y - Shoulder_Right.transform.position.y;
			float dz = listCollider[1].transform.position.z - Shoulder_Right.transform.position.z;
			
			Debug.LogWarning("ShoulderR/Cube R : dX ("+dx+")  dY ("+dy+")  dZ ("+dz+")");
		}
}
